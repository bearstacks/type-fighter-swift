//
//  Counting.m
//  MemorySwipe
//
//  Created by Rehan Anwar on 2015-01-26.
//  Copyright (c) 2015 Rehan Anwar. All rights reserved.
//

#import "Counting.h"

@implementation Counting

-(id)initWithColor:(UIColor*)color radius:(CGFloat)radius location:(CGPoint)location height:(CGFloat)height width:(CGFloat)width number:(int)number{
    
    if (number==3) {
            Counting *number3=[self number3:color radius:radius location:location height:height width:width];
            [number3 addAnimation:[self animation] forKey:nil];
        
            return number3;
    }
    else if (number==2)
    {
        Counting *number2=[self number2:color radius:radius location:location height:height width:width];
        [number2 addAnimation:[self animation] forKey:nil];
        
        return number2;
    }
    
    else
    {
        Counting *number1=[self number1:color radius:radius location:location height:height width:width];
        [number1 addAnimation:[self animation] forKey:nil];
        
        return number1;
    }
}

-(Counting *)number3:(UIColor*)color radius:(CGFloat)radius location:(CGPoint)location height:(CGFloat)height width:(CGFloat)width{
    
    //Declare the Path
    UIBezierPath *aPath = [UIBezierPath bezierPathWithArcCenter:CGPointMake(location.x, location.y)
                                                         radius:radius
                                                     startAngle:-M_PI/2-0.7
                                                       endAngle:M_PI/2
                                                      clockwise:YES];
    
    [aPath addArcWithCenter:CGPointMake(location.x, location.y+radius*2) radius:radius startAngle:-M_PI/2 endAngle:M_PI/2+0.7 clockwise:YES];
    
    
    //Declare the layer
    Counting* layer=[self layer];
    layer.strokeColor = color.CGColor;
    layer.path=aPath.CGPath;
    
    return  layer;
}

-(Counting *)number2:(UIColor*)color radius:(CGFloat)radius location:(CGPoint)location height:(CGFloat)height width:(CGFloat)width{
    
    //Declare the Path
    UIBezierPath *aPath = [UIBezierPath bezierPathWithArcCenter:CGPointMake(location.x, location.y)
                                                         radius:radius
                                                     startAngle:-M_PI/2-0.6
                                                       endAngle:M_PI/2-0.7
                                                      clockwise:YES];
    
    [aPath addLineToPoint:CGPointMake(location.x-10, location.y+radius+15)];
    [aPath addLineToPoint:CGPointMake(location.x+25, location.y+radius+15)];
    
    //Declare the layer
    Counting* layer=[self layer];
    layer.strokeColor = color.CGColor;
    layer.path=aPath.CGPath;
    
    return  layer;
    
}

-(Counting *)number1:(UIColor*)color radius:(CGFloat)radius location:(CGPoint)location height:(CGFloat)height width:(CGFloat)width{
    
    //Declare the Path
    UIBezierPath *aPath = [UIBezierPath bezierPath];
    [aPath moveToPoint:CGPointMake(location.x, location.y-20)];
    [aPath addLineToPoint:CGPointMake(location.x, location.y+40)];
    
    //Declare the layer
    Counting* layer=[self layer];
    layer.strokeColor = color.CGColor;
    layer.path=aPath.CGPath;
    
    return  layer;
    
}

-(Counting *)layer{
    Counting* drawingPathLayer=[Counting layer];
    drawingPathLayer.fillColor = nil;
    drawingPathLayer.lineWidth = 20.0f;
    drawingPathLayer.opacity = 1;
    drawingPathLayer.lineJoin = kCALineJoinRound;
    drawingPathLayer.lineCap = kCALineCapRound;
    
    return  drawingPathLayer;
}

-(CABasicAnimation*)animation{
    CABasicAnimation *drawAnimation = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
    drawAnimation.duration            = 0.5; // "animate over 10 seconds or so.."
    drawAnimation.repeatCount         = 1;  // Animate only once..
    
    // Animate from no part of the stroke being drawn to the entire stroke being drawn
    drawAnimation.fromValue = [NSNumber numberWithFloat:0.0f];
    drawAnimation.toValue   = [NSNumber numberWithFloat:1.0f];
    return drawAnimation;
}


@end
