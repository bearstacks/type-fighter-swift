//
//  GameEndViewController.swift
//  Type Fighter
//
//  Created by Muhammad Hassaan Khawar on 2016-03-17.
//  Copyright © 2016 BearStacks Inc. All rights reserved.
//

import Foundation
import UIKit

protocol GameEndViewControllerDelegate : NSObjectProtocol {
    func restartGame()
}

class GameEndViewController: UIViewController {
    
    let maxDefinitionLabelHeight : CGFloat = 150
    
    var delegate : GameEndViewControllerDelegate?
    
    @IBOutlet weak var wordLabel            : UILabel!
    @IBOutlet weak var wordDefinitionLabel  : UILabel!
    @IBOutlet weak var scoreLabel           : UILabel!
    @IBOutlet weak var restartButton        : UIButton!
    @IBOutlet weak var mainMenuButton       : UIButton!
    
    var score       : Int!
    var wordNode    : ASAttributedLabelNode!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if UIDevice().isIpad() {
            wordLabel.font = UIFont(name: wordLabel.font!.fontName, size: 60)
            scoreLabel.font = UIFont(name: scoreLabel.font!.fontName, size: 70)
            restartButton.titleLabel!.font = UIFont(name: restartButton.titleLabel!.font!.fontName, size: 50)
            mainMenuButton.titleLabel!.font = UIFont(name: mainMenuButton.titleLabel!.font!.fontName, size: 50)
        }
        
        self.updateView()
    }
    
    func updateView() {
        if wordNode?.type == .Special {
            self.wordLabel.text = wordNode.word
            self.wordDefinitionLabel.text = specialWordMessages.randomValue()
        } else if wordNode?.type == .Normal {
            self.wordLabel.text = wordNode?.word
            self.wordDefinitionLabel.text = normalWordMessages.randomValue()
        } else if wordNode == nil {
            self.wordLabel.text = "YOU LOSE"
            self.wordDefinitionLabel.text = noWordMessages.randomValue()
        }
        
        self.scoreLabel.text = "Score: \(score)"
        
        let word : NSString = wordDefinitionLabel.text!
        var fontSize : CGFloat = UIDevice().isIpad() ? 30 : 21
        var boundingRect = word.boundingRectWithSize(CGSizeMake(wordDefinitionLabel.frame.width, 10000), options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes: [NSFontAttributeName: wordDefinitionLabel.font], context: nil)
        
        while boundingRect.height > maxDefinitionLabelHeight {
            fontSize = fontSize - 2
            boundingRect = word.boundingRectWithSize(CGSizeMake(wordDefinitionLabel.frame.width, 10000), options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes: [NSFontAttributeName: UIFont(name: "Lilly", size: fontSize)!], context: nil)
        }
        
        wordDefinitionLabel.font = UIFont(name: "Lilly", size: fontSize)
    }
    
    @IBAction func restartButtonPressed(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
        self.delegate?.restartGame()
    }
    
    @IBAction func mainMenuButtonPressed(sender: AnyObject) {
        self.navigationController?.popToRootViewControllerAnimated(true)
    }
}
