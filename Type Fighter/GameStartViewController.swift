//
//  GameStartViewController.swift
//  Type Fighter
//
//  Created by Muhammad Hassaan Khawar on 2016-03-17.
//  Copyright © 2016 BearStacks Inc. All rights reserved.
//

import UIKit
import Foundation
import GameKit

class GameStartViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate, GKGameCenterControllerDelegate {
    
    var textField   : UITextField!
    
    var gameViewController      : GameViewController? = nil
    
    @IBOutlet weak var startGameButton: UIButton!
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var highScoreLabel: UILabel!
    @IBOutlet weak var difficultyPickerContainerView: UIView!
    @IBOutlet weak var volumeButton: UIButton!
    @IBOutlet weak var difficultyLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController!.setNavigationBarHidden(true, animated: false)
        UIApplication.sharedApplication().applicationSupportsShakeToEdit = false
        
        self.authenticateLocalPlayer()
        
        if UIDevice().isIpad() {
            startGameButton.titleLabel?.font = UIFont(name: startGameButton.titleLabel!.font!.fontName, size: 100)
            difficultyLabel.font = UIFont(name: difficultyLabel.font!.fontName, size: 50)
            highScoreLabel.font = UIFont(name: highScoreLabel.font!.fontName, size: 40)
        }
        
        if DeviceType.IS_IPHONE_5 || DeviceType.IS_IPHONE_4_OR_LESS {
            startGameButton.titleLabel?.font = UIFont(name: startGameButton.titleLabel!.font!.fontName, size: 60)
        }
        
        //Add notification and dummy textfield to get keyboard height for current device
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(GameStartViewController.keyboardWillChangeFrameNotification(_:)), name: UIKeyboardWillShowNotification, object: nil)
        textField = UITextField()
        self.view.addSubview(textField)
        textField.autocorrectionType = .No
        textField.autocapitalizationType = .None
        textField.becomeFirstResponder()

        pickerView.selectRow(1, inComponent: 0, animated: true)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        startGameButton.layer.cornerRadius = 30
        startGameButton.clipsToBounds = true
        
        highScoreLabel.layer.cornerRadius = 30
        highScoreLabel.clipsToBounds = true
                
        difficultyPickerContainerView.layer.cornerRadius = 30
        difficultyPickerContainerView.clipsToBounds = true
        
        if NSUserDefaults.standardUserDefaults().boolForKey(VolumeUserDefaultKey) == true {
            volumeButton.setBackgroundImage(UIImage(named: "volume_on"), forState: .Normal)
        } else {
            volumeButton.setBackgroundImage(UIImage(named: "volume_off"), forState: .Normal)
        }
        
        updateHighScoreLabel()
    }
    
    override func viewWillDisappear(animated: Bool) {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    func keyboardWillChangeFrameNotification(notification: NSNotification) {
        let keyboardSize = (notification.userInfo![UIKeyboardFrameEndUserInfoKey] as! NSValue).CGRectValue().size
        DataManager.sharedInstance.keyBoardHeight = keyboardSize.height
        
        textField.resignFirstResponder()
        textField.removeFromSuperview()
    }
    
    func gameCenterViewControllerDidFinish(gameCenterViewController: GKGameCenterViewController) {
        gameCenterViewController.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func updateHighScoreLabel() {
        let userDefaults = NSUserDefaults.standardUserDefaults()
        let highScore = userDefaults.integerForKey(HighScoreUserDefaultKeys[pickerView.selectedRowInComponent(0)])
        
        highScoreLabel.text = "HighScore: \(highScore)"
    }
    
    func authenticateLocalPlayer() {
        let localPlayer: GKLocalPlayer = GKLocalPlayer.localPlayer()
        
        localPlayer.authenticateHandler = {(ViewController, error) -> Void in
            if((ViewController) != nil) {
                // 1 Show login if player is not logged in
                self.presentViewController(ViewController!, animated: true, completion: nil)
            } else if (localPlayer.authenticated) {
                // 2 Player is already euthenticated & logged in, load game center
                DataManager.sharedInstance.gcEnabled = true
            } else {
                // 3 Game center is not enabled on the users device
                DataManager.sharedInstance.gcEnabled = false
                print("Local player could not be authenticated, disabling game center")
                print(error)
            }
        }
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return Difficulties.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return Difficulties[row]
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        updateHighScoreLabel()
    }
    
    func pickerView(pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusingView view: UIView?) -> UIView {
        
        var pickerLabel = view as? UILabel
        
        if (pickerLabel == nil)
        {
            pickerLabel = UILabel()
            
            let fontSize : CGFloat = UIDevice().isIpad() ? 50 : 30
            
            pickerLabel!.font = UIFont(name: "PlaytimeWithHotToddies", size: fontSize)
            pickerLabel?.textColor = UIColor(red: 255.0/255.0, green: 200.0/255.0, blue: 25.0/255.0, alpha: 1.0)
            pickerLabel?.textAlignment = NSTextAlignment.Center
        }
        
        pickerLabel?.text = Difficulties[row]
        
        return pickerLabel!
    }
    
    func pickerView(pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return UIDevice().isIpad() ? 50 : 37
    }
    
    @IBAction func startGameButtonPressed(sender: AnyObject) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        gameViewController = storyboard.instantiateViewControllerWithIdentifier("GameViewController") as? GameViewController
        
        switch pickerView.selectedRowInComponent(0) {
        case 0:
            gameViewController!.difficulty = .Easy
            break
        case 1:
            gameViewController!.difficulty = .Medium
            break
        case 2:
            gameViewController!.difficulty = .Hard
            break
        default:
            break
        }
        
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = kCATransitionFade
        transition.subtype = kCATransitionFromTop
        
        self.navigationController?.view.layer.addAnimation(transition, forKey: "push")
        self.navigationController?.pushViewController(gameViewController!, animated: true)
    }
    
    @IBAction func volumeButtonPressed(sender: AnyObject) {
         NSUserDefaults.standardUserDefaults().setBool(!NSUserDefaults.standardUserDefaults().boolForKey(VolumeUserDefaultKey), forKey: VolumeUserDefaultKey)
        
        if NSUserDefaults.standardUserDefaults().boolForKey(VolumeUserDefaultKey) == true {
            volumeButton.setBackgroundImage(UIImage(named: "volume_on"), forState: .Normal)
        } else {
            volumeButton.setBackgroundImage(UIImage(named: "volume_off"), forState: .Normal)
        }
    }
    
    @IBAction func creditsButtonPressed(sender: AnyObject) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let creditsViewController = storyboard.instantiateViewControllerWithIdentifier("CreditsViewController") as? CreditsViewController
        self.presentViewController(creditsViewController!, animated: true, completion: nil)
    }
}