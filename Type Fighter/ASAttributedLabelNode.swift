//
//  ASAttributedLabelNode.swift
//
//  Created by Alex Studnicka on 15/08/14.
//  Copyright © 2016 Alex Studnicka. MIT License.
//

import UIKit
import SpriteKit

class ASAttributedLabelNode: SKSpriteNode {
    
    var word        : String!
    var typedWord   : String!
    var type        : WordType?
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
	}
	
    init(size: CGSize, word : String, wordType : WordType = .Normal) {
		super.init(texture: nil, color: .clearColor(), size: size)
        self.name = word
        self.word = word
        self.type = wordType
        if type == .Special {
            self.color = UIColor.blackColor()
        }
        updateTypedWord("")
	}
    
	var attributedString: NSAttributedString! {
		didSet {
			draw()
		}
	}
    
    func updateTypedWord(typedWord : String) {
        self.typedWord = typedWord
        
        let normalWordInitialColor  = UIColor.init(red: 255.0/255.0, green: 102.0/255.0, blue: 102.0/255.0, alpha: 1.0)
        let normalWordTypedColor    = UIColor.greenColor()
        let specialWordInitialColor = UIColor.yellowColor()
        let specialWordTypedColor   = UIColor.orangeColor()
        
        let attributedString = NSMutableAttributedString(string: word, attributes: [NSFontAttributeName: WordsFont!])
        
        let color = type == .Normal ? normalWordInitialColor : specialWordInitialColor
        attributedString.addAttribute(NSForegroundColorAttributeName, value: color, range: NSMakeRange(typedWord.characters.count, word.characters.count-typedWord.characters.count))

        let typedColor = type == .Normal ? normalWordTypedColor : specialWordTypedColor
        attributedString.addAttribute(NSForegroundColorAttributeName, value: typedColor, range: NSMakeRange(0, typedWord.characters.count))
        self.attributedString = attributedString
    }
	
	func draw() {
		guard let attrStr = attributedString else {
			texture = nil
			return
		}
		
		let scaleFactor = UIScreen.mainScreen().scale
		let colorSpace = CGColorSpaceCreateDeviceRGB()
		let bitmapInfo = CGImageAlphaInfo.PremultipliedLast.rawValue
		guard let context = CGBitmapContextCreate(nil, Int(size.width * scaleFactor), Int(size.height * scaleFactor), 8, Int(size.width * scaleFactor) * 4, colorSpace, bitmapInfo) else {
			return
		}

		CGContextScaleCTM(context, scaleFactor, scaleFactor)
		CGContextConcatCTM(context, CGAffineTransform(a: 1, b: 0, c: 0, d: -1, tx: 0, ty: size.height))
		UIGraphicsPushContext(context)
		
		let strHeight = attrStr.boundingRectWithSize(size, options: .UsesLineFragmentOrigin, context: nil).height
		let yOffset = (size.height - strHeight) / 2.0
		attrStr.drawWithRect(CGRect(x: 0, y: yOffset, width: size.width, height: strHeight), options: .UsesLineFragmentOrigin, context: nil)
		
		if let imageRef = CGBitmapContextCreateImage(context) {
			texture = SKTexture(CGImage: imageRef)
		} else {
			texture = nil
		}
		
		UIGraphicsPopContext()
	}
	
}
