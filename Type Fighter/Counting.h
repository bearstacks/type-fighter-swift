//
//  Counting.h
//  MemorySwipe
//
//  Created by Rehan Anwar on 2015-01-26.
//  Copyright (c) 2015 Rehan Anwar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Counting : CAShapeLayer


-(id)initWithColor:(UIColor*)color radius:(CGFloat)radius location:(CGPoint)location height:(CGFloat)height width:(CGFloat)width number:(int)number;
@end

