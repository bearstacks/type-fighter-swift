//
//  DataManager.swift
//  Type Fighter
//
//  Created by Muhammad Hassaan Khawar on 2016-03-27.
//  Copyright © 2016 BearStacks Inc. All rights reserved.
//

import UIKit

class DataManager {
    
    static let sharedInstance = DataManager()
    
    var keyBoardHeight : CGFloat = 225
    
    var gcEnabled   : Bool = false
    
    private init() {}

}
