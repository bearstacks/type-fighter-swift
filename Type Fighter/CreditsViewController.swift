//
//  CreditsViewController.swift
//  Type Fighter
//
//  Created by Muhammad Hassaan Khawar on 2016-04-26.
//  Copyright © 2016 BearStacks Inc. All rights reserved.
//

import UIKit

class CreditsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!

    let credits = ["Images designed by Freepik"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.separatorStyle = .None
        tableView.allowsSelection = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func backButtonPressed(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: {
        })
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return credits.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCellWithIdentifier("cell")
        
        if cell == nil {
            cell = UITableViewCell(frame: CGRectMake(0, 0, self.view.frame.width, 40))
        }
        
        let label = UILabel(frame: CGRectMake(3, 0, tableView.frame.width - 6, cell!.frame.height))
        label.text = credits[indexPath.row]
        label.textColor = UIColor(red: 102.0/255.0, green: 102.0/255.0, blue: 102.0/255.0, alpha: 1)
        label.font = UIFont(name: "PlaytimeWithHotToddies", size: 18)
        
        cell?.addSubview(label)
        
        return cell!
    }
}
