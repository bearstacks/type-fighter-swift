//
//  GameViewController.swift
//  Type Fighter
//
//  Created by Muhammad Hassaan Khawar on 2016-03-02.
//  Copyright (c) 2016 BearStacks Inc. All rights reserved.
//

import UIKit
import SpriteKit

class GameViewController: UIViewController, GameEndViewControllerDelegate, GameSceneDelegate {
    
    var gameScene   : GameScene!
    var skView      : SKView!
    
    var difficulty  : Difficulty!
    
    var gameEnded : Bool = false
    
    var gameEndViewController : GameEndViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        gameScene = GameScene(size: view.bounds.size, difficulty: difficulty)
        gameScene.sceneDelegate = self
        skView = view as! SKView
        skView.showsFPS = false
        skView.showsNodeCount = false
        skView.ignoresSiblingOrder = true
        gameScene.scaleMode = .ResizeFill
        skView.presentScene(gameScene)
    }
    
    override func viewDidDisappear(animated: Bool) {
        self.gameScene.removeFromParent()
        self.skView.presentScene(nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        print("didReceiveMemoryWarning")
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    //GameSceneDelegate
    func gameEnded(score : Int, wordNode : ASAttributedLabelNode?) {
        gameEnded = true
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        gameEndViewController = storyboard.instantiateViewControllerWithIdentifier("GameEndViewController") as? GameEndViewController
        gameEndViewController?.delegate = self
        gameEndViewController?.score = score
        gameEndViewController?.wordNode = wordNode
        
        self.navigationController?.pushViewController(gameEndViewController!, animated: true)
    }
    
    //GameEndViewControllerDelegate
    func restartGame() {
        skView.presentScene(gameScene)
    }
}
