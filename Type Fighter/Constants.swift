//
//  Constants.swift
//  Type Fighter
//
//  Created by Muhammad Hassaan Khawar on 2016-03-23.
//  Copyright © 2016 BearStacks Inc. All rights reserved.
//

import UIKit

//Generic
struct PhysicsCategory {
    static let None         : UInt32 = 0
    static let All          : UInt32 = UInt32.max
    static let Word         : UInt32 = 0b1
    static let Projectile   : UInt32 = 0b10
}

enum WordType {
    case Normal
    case Special
}

enum Difficulty {
    case Easy
    case Medium
    case Hard
}

let Difficulties = ["Easy", "Medium", "Hard"]

//NSUserDefaults
let HighScoreUserDefaultKeys    = ["HighScoreEasy", "HighScoreMedium", "HighScoreHard"]
let VolumeUserDefaultKey        = "VolumeUserDefaultKey"
let gameCenterLeaderBoardKeys   = ["easy_leaderboard", "medium_leaderboard", "hard_leaderboard"]

//Game Start View Controller

//Game View Controller
let WordsFont           = UIDevice().isIpad() ? UIFont(name: "PlaytimeWithHotToddies", size: 45) :
                                                UIFont(name: "PlaytimeWithHotToddies", size: 30)
let gamePageLbelFont    = UIDevice().isIpad() ? UIFont(name: "orangejuice", size: 35) :
                                                UIFont(name: "orangejuice", size: 24)
let gamePageLabelColor  = UIColor.blackColor()

//Game End View Controller
let specialWordMessages     = [ "Special words are optional. They can be very helpful, but it comes with a cost",
                                "You didn't have to start typing the special word if you weren't planning on completing it, you know.",
                                "Haha! That special word got you didn't it :p",
                                "Those special words are just so tempting to type aren't they?",
                                "Those capital letters just aren't fair",
                                "Typing a special word correctly removes all words on the screen.",
                                "Typing a special word correctly removes all words on the screen.",
                                "Making even a single mistake while typing a special word will immediately end your game.",
                                "Making even a single mistake while typing a special word will immediately end your game.",
                                "Making even a single mistake while typing a special word will immediately end your game.",
                                "Typing a special word correctly removes all words on the screen.",
                                "Special words are optional. They can be very helpful, but it comes with a cost",
                                "Special words are optional. They can be very helpful, but it comes with a cost"]
let noWordMessages          = [ "Well thats a pity... weren't event typing a word and you lost.",
                                "Better luck next time.",
                                "What a shame...",
                                "'Fool me once, shame on... shame on you. Fool me... You can't get fooled again!'",
                                "I know you can do better than that.",
                                "Weak effort.",
                                "That was pretty good... for a two year-old. >.>",
                                "Next time, try using the keyboard"]
let normalWordMessages      = [ "Give that one another try, you're getting better",
                                "Come on, work those fingers!",
                                "Don't let those words get too close to you now.",
                                "At this rate, we're never getting to that highscore.",
                                "Well there's always room for improvement, I guess.",
                                "Dont worry, Rome wasn't built in a day either.",
                                "Keep it going... practice makes perfect!",
                                "Don't let that word ever do that to you again!",
                                "It's okay, we'll just blame the keyboard",
                                "Look at that word above, and study it for next time."]
