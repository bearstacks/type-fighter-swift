//
//  GameScene.swift
//  Type Fighter
//
//  Created by Muhammad Hassaan Khawar on 2016-03-02.
//  Copyright (c) 2016 BearStacks Inc. All rights reserved.
//

import SpriteKit
import AudioToolbox
import GameKit

let GenerateWordsActionKey = "GenerateWordsAction"

protocol GameSceneDelegate : NSObjectProtocol {
    func gameEnded(score : Int, wordNode : ASAttributedLabelNode?)
}

class GameScene: SKScene, SKPhysicsContactDelegate {
    
    let intervalBetweenSpecialWords : CFTimeInterval = 15.0
    let perfectWordStreakValue      : Int = 5
    let timeAllowedForTapping       : NSTimeInterval = 5.0
    
    let countdownColors : [UIColor] = [UIColor.blueColor() ,UIColor.redColor(), UIColor.yellowColor(), UIColor.greenColor()]
    
    var sceneDelegate : GameSceneDelegate?
    
    var player              : SKSpriteNode!
    var arrow               : SKSpriteNode!
    var tapLabel            : SKSpriteNode!
    
    var scoreLabel          : SKLabelNode!
    var perfectWordsLabel   : SKLabelNode!
    
    var backgroundMusic     : SKAudioNode!
    var tappingMusic        : SKAudioNode!
    
    var generateWordsAction : SKAction!
    
    var difficulty  : Difficulty
    var textField   : UITextField!
    
    var normalWordsList     : [String] = [String]()
    var specialWordsList    : [String] = [String]()
    var previousWords       : [String] = [String]()
    var currentWords        : [String] = [String]()

    var currentlyTypedWordNode      : ASAttributedLabelNode?
    var previouslyTypedWordNode     : ASAttributedLabelNode?
    var specialWordNode             : ASAttributedLabelNode?
    
    var wordsCompleted                      : UInt = 0
    var level                               : UInt = 0
    var score                               : Int!
    var currentScoreMilestone               : Int!
    var scoreMilestoneIncrement             : Int!
    var bonusScoreForPerfectWord            : Int!
    var perfectWordStreak                   : Int!
    var delayBetweenWords                   : Double!
    var durationForWordsComingDown          : Double!
    var durationForSpecialWordsComingDown   : Double!
    
    var isPerfectWord       : Bool = true
    var allowTapping        : Bool = false
    var createSpecialWord   : Bool = false
    
    var specialWordTimer    : NSTimer?
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    init(size: CGSize, difficulty : Difficulty) {
        self.difficulty = difficulty
        super.init(size: size)
        
        //Read contents of word file
        do {
            try readFile()
        } catch {
            print("Unable to read file")
        }
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(GameScene.keyboardWillHide), name: UIKeyboardWillHideNotification, object: nil)
    }

    override func didMoveToView(view: SKView) {
        
        let background = SKSpriteNode(imageNamed: "background_14")
        background.position = CGPoint(x: frame.size.width / 2, y: frame.size.height / 2)
        background.zPosition = -1
        background.size = view.frame.size
        addChild(background)
        
        setUpNewGame()
        runAction(SKAction.sequence([
            SKAction.waitForDuration(0.5),
            SKAction.runBlock({ self.countdown(3) })
            ]))
        
        backgroundColor = SKColor.whiteColor()
        
        backgroundMusic = SKAudioNode(fileNamed: "background-music-aac.caf")
        
        tappingMusic = SKAudioNode(fileNamed: "perfect_word_tapping.wav")
        
        //Setup physics world
        physicsWorld.gravity = CGVectorMake(0, 0)
        physicsWorld.contactDelegate = self
        
        //Setup Textfield used for detecting typing
        textField = UITextField(frame: CGRectMake(0, 0, 0, 0))
        textField.text = ""
        self.view?.addSubview(textField)
        textField.autocorrectionType = .No
        textField.autocapitalizationType = .None
        textField.becomeFirstResponder()
        textField.addTarget(self, action: #selector(GameScene.textChanged), forControlEvents: UIControlEvents.EditingChanged)
        
        //Add score label to scene
        var labelText = "Score: 000"
        var labelSize = labelText.boundingRectWithSize(CGSizeMake(1000, 30), options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes: [NSFontAttributeName: gamePageLbelFont!], context: nil).size
    
        scoreLabel = SKLabelNode()
        updateScore(0)
        scoreLabel.position = CGPointMake(labelSize.width/2, DataManager.sharedInstance.keyBoardHeight + labelSize.height/2 + 30)
        scoreLabel.fontColor = gamePageLabelColor
        scoreLabel.fontSize = gamePageLbelFont!.pointSize
        scoreLabel.fontName = gamePageLbelFont!.fontName
        addChild(scoreLabel)
        
        //Add tap image
        tapLabel = SKSpriteNode(imageNamed: "tap")
        tapLabel.size = CGSizeMake(200, 80)
        tapLabel.position = CGPointMake(self.view!.frame.width/2, (self.view!.frame.height + DataManager.sharedInstance.keyBoardHeight)/2)
        tapLabel.alpha = 0.0
        addChild(tapLabel)
        
        //Add Perfect word streak label to scene
        labelText = "Perfect word Streak: 000"
        labelSize = labelText.boundingRectWithSize(CGSizeMake(1000, 30), options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes: [NSFontAttributeName: gamePageLbelFont!], context: nil).size
        
        perfectWordsLabel = SKLabelNode()
        updatePerfectWordStreak(0)
        perfectWordsLabel.position = CGPointMake(labelSize.width/2, DataManager.sharedInstance.keyBoardHeight + labelSize.height/2)
        perfectWordsLabel.fontColor = gamePageLabelColor
        perfectWordsLabel.fontSize = gamePageLbelFont!.pointSize
        perfectWordsLabel.fontName = gamePageLbelFont!.fontName
        addChild(perfectWordsLabel)
        
        //Add player node to scene
        player = SKSpriteNode(imageNamed: "rocket")
        player.size = UIDevice().isIpad() ? CGSizeMake(55, 100) : CGSizeMake(35, 60)
        player.position = CGPointMake(size.width * 0.5, DataManager.sharedInstance.keyBoardHeight + player.size.height/2 + 35)
        addChild(player)
    }
    
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
    }
    
    func keyboardWillHide() {
        textField.becomeFirstResponder()
    }
    
    func setCreateSpecialWord() {
        createSpecialWord = true
    }
    
    func countdown(number : Int32) {
        let countdownNumber = Counting(color: countdownColors[Int(number)], radius: 20, location: CGPointMake(view!.center.x, (view!.frame.height - DataManager.sharedInstance.keyBoardHeight)/2), height: 50, width: 50, number: number)
        view?.layer.addSublayer(countdownNumber)
        
        if number > 1 {
            //Continue counting down to 1
            let countingAction = SKAction.sequence([
                SKAction.waitForDuration(1.0),
                SKAction.runBlock(countdownNumber.removeFromSuperlayer),
                SKAction.runBlock({ self.countdown(number-1) })
                ])
            runAction(countingAction)
            
        } else {
            let startGameAction = SKAction.sequence([
                SKAction.waitForDuration(1.0),
                SKAction.runBlock(countdownNumber.removeFromSuperlayer),
                SKAction.runBlock(startNewGame)
                ])
            runAction(startGameAction)
        }
    }
    
    func startNewGame() {
        if NSUserDefaults.standardUserDefaults().boolForKey(VolumeUserDefaultKey) == true {
            let tempAudio = SKAudioNode(fileNamed: "background-music-aac.caf")
            tempAudio.autoplayLooped = true
            backgroundMusic = tempAudio
            addChild(tempAudio)
        }
        
        //Start running action to keep adding new words to scene
        generateWordsAction = SKAction.repeatActionForever(
            SKAction.sequence([
                SKAction.runBlock(addWord),
                SKAction.waitForDuration(delayBetweenWords)
                ])
        )
        runAction(generateWordsAction, withKey: GenerateWordsActionKey)
        
        specialWordTimer = NSTimer.scheduledTimerWithTimeInterval(intervalBetweenSpecialWords, target: self, selector: #selector(GameScene.setCreateSpecialWord), userInfo: nil, repeats: true)
    }
    
    func setUpNewGame() {
        //Set up all variables for a new game
        
        previousWords.removeAll()
        currentWords.removeAll()
        
        currentlyTypedWordNode = nil
        previouslyTypedWordNode = nil
        specialWordNode = nil
        
        wordsCompleted = 0
        score = 0
        perfectWordStreak = 0
        isPerfectWord = true
        allowTapping = false
        createSpecialWord = false
        
        switch difficulty {
        case .Easy:
            scoreMilestoneIncrement = 25
            currentScoreMilestone = scoreMilestoneIncrement
            delayBetweenWords = 2.5
            durationForWordsComingDown = 20.0
            durationForSpecialWordsComingDown = 15.0
            bonusScoreForPerfectWord = 2
            break
        case .Medium:
            scoreMilestoneIncrement = 40
            currentScoreMilestone = scoreMilestoneIncrement
            delayBetweenWords = 2.0
            durationForWordsComingDown = 13.0
            durationForSpecialWordsComingDown = 10.0
            bonusScoreForPerfectWord = 3
            break
        case .Hard:
            scoreMilestoneIncrement = 50
            currentScoreMilestone = scoreMilestoneIncrement
            delayBetweenWords = 1.5
            durationForWordsComingDown = 9.0
            durationForSpecialWordsComingDown = 6.0
            bonusScoreForPerfectWord = 5
            break
        }
    }
    
    func readFile() throws {
        var fileName = ""
        switch difficulty {
        case .Easy:
            fileName = "easy_words"
            break
        case .Medium:
            fileName = "medium_words"
            break
        case .Hard:
            fileName = "hard_words"
            break
        }
        if let path = NSBundle.mainBundle().pathForResource(fileName, ofType: "txt") {
            let data = try String(contentsOfFile: path, encoding: NSUTF8StringEncoding)
            if data != "" {
                normalWordsList = data.componentsSeparatedByCharactersInSet(NSCharacterSet.newlineCharacterSet())
            }
        }
        
        if let path = NSBundle.mainBundle().pathForResource("special_words", ofType: "txt") {                        let data = try String(contentsOfFile: path, encoding: NSUTF8StringEncoding)
            if data != "" {
                specialWordsList = data.componentsSeparatedByCharactersInSet(NSCharacterSet.newlineCharacterSet())
            }
        }
    }
    
    func updateScore(scoreGained : Int) {
        score = score + scoreGained
        
        var scoreString = String(score)
        for _ in String(score).characters.count ..< 3 {
            scoreString += " "
        }
        
        scoreLabel.text = "Score: \(scoreString)"
        
        if score > currentScoreMilestone {
            currentScoreMilestone = currentScoreMilestone + scoreMilestoneIncrement
            delayBetweenWords = delayBetweenWords - (delayBetweenWords/10)
            durationForWordsComingDown = durationForWordsComingDown - (durationForWordsComingDown/10)
            durationForSpecialWordsComingDown = durationForSpecialWordsComingDown - (durationForSpecialWordsComingDown/10)
            
            removeActionForKey(GenerateWordsActionKey)
            
            generateWordsAction = SKAction.repeatActionForever(
                SKAction.sequence([
                    SKAction.waitForDuration(delayBetweenWords),
                    SKAction.runBlock(addWord)
                    ])
            )
            
            runAction(generateWordsAction, withKey: GenerateWordsActionKey)

        }
    }
    
    func updatePerfectWordStreak(streak : Int) {
        perfectWordStreak = streak
        
        var streakString = String(streak)
        for _ in String(streak).characters.count ..< 3 {
            streakString += " "
        }
        
        perfectWordsLabel.text = "Perfect word streak: \(streakString)"
    }
    
    //Add word node object to scene
    func addWord() {
        let newWord = getNewWord()
        var wordType : WordType
        
        if createSpecialWord == true {
            wordType = .Special
            createSpecialWord = false
        } else {
            wordType = .Normal
        }
        
        let wordLabel = ASAttributedLabelNode(size: newWord.boundingRectWithSize(CGSizeMake(1000, 30), options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes: [NSFontAttributeName: WordsFont!], context: nil).size, word: newWord, wordType: wordType)
        let randomXPosition = random(min: wordLabel.frame.size.width/2, max: size.width - wordLabel.frame.size.width/2)
        
        wordLabel.position = CGPointMake(randomXPosition, size.height + wordLabel.frame.size.height/2)
        wordLabel.physicsBody = SKPhysicsBody(rectangleOfSize: wordLabel.frame.size)
        wordLabel.physicsBody!.dynamic = true
        wordLabel.physicsBody!.categoryBitMask = PhysicsCategory.Word
        wordLabel.physicsBody!.contactTestBitMask = PhysicsCategory.Projectile
        wordLabel.physicsBody!.collisionBitMask = PhysicsCategory.None
        
        addChild(wordLabel)
        
        var actionMove = SKAction.moveTo(CGPoint(x: randomXPosition, y: player.position.y), duration: NSTimeInterval(durationForWordsComingDown))
        if wordType == .Special {
            specialWordNode = wordLabel
            actionMove = SKAction.moveTo(CGPoint(x: randomXPosition, y: player.position.y), duration: NSTimeInterval(durationForSpecialWordsComingDown))
            let loseAction = SKAction.runBlock() {
                self.specialWordEnded()
            }
            wordLabel.runAction(SKAction.sequence([actionMove, loseAction]))
        } else {
            let loseAction = SKAction.runBlock() {
                self.gameEnded()
            }
            wordLabel.runAction(SKAction.sequence([actionMove, loseAction]))
        }
    }
    
    //Find a new word from the txt file
    func getNewWord() -> String {
        var word : String!
        var validWord = false
        
        if createSpecialWord {
            while !validWord {
                word = specialWordsList.randomValue()
                validWord = true
                
                //Make sure new word is not the same as any currently existing word on the scene
                for value in currentWords {
                    if value[value.startIndex] == word[word.startIndex] {
                        validWord = false
                        break
                    }
                }
            }            
        }
        
        while !validWord {
            word = normalWordsList.randomValue().lowercaseString
            validWord = true
            
            //Make sure new word is not the same as any currently existing word on the scene
            for value in currentWords {
                if value[value.startIndex] == word[word.startIndex] {
                    validWord = false
                    break
                }
            }
        }
        
        currentWords.append(word)
        previousWords.append(word)
        
        return word
    }
    
    func textChanged() {
        //Backspace pressed (not allowed)
        if currentlyTypedWordNode != nil && currentlyTypedWordNode?.typedWord.characters.count > textField.text?.characters.count {
            AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
            textField.text = currentlyTypedWordNode?.typedWord
            isPerfectWord = false
        }
        
        //First character of new word being typed out
        else if textField.text!.characters.count == 1 {
            for word in currentWords {
                if word[word.startIndex] == textField.text![textField.text!.startIndex] {
                    currentlyTypedWordNode = self.childNodeWithName(word) as? ASAttributedLabelNode
                    if currentlyTypedWordNode != nil {
                        currentlyTypedWordNode!.updateTypedWord(textField.text!)
                        isPerfectWord = true
                    } else {
                        for node in self.children {
                            if node.name == word {
                                currentlyTypedWordNode = node as? ASAttributedLabelNode
                                currentlyTypedWordNode!.updateTypedWord(textField.text!)
                                isPerfectWord = true
                            }
                        }
                    }
                }
            }
            if currentlyTypedWordNode == nil {
                textField.text = ""
            }
        }
        
        //New character is typed
        else {
            
            //Word completely typed out
            if textField.text == currentlyTypedWordNode!.name {
                currentlyTypedWordNode?.updateTypedWord(textField.text!)
                textField.text = ""
                
                if isPerfectWord {
                    updatePerfectWordStreak(perfectWordStreak+1)
                } else {
                    updatePerfectWordStreak(0)
                }
                
                currentWords.removeAtIndex(currentWords.indexOf(currentlyTypedWordNode!.name!)!)
                previouslyTypedWordNode = currentlyTypedWordNode
                currentlyTypedWordNode = nil
                removeWordNode(previouslyTypedWordNode!)
                return
            }
            
            let index = textField.text!.endIndex.predecessor()
            
            //Correct character typed
            if textField.text![index] == currentlyTypedWordNode!.name![index] {
                currentlyTypedWordNode?.updateTypedWord(textField.text!)
            }
            //Incorrect character typed
            else {
                if currentlyTypedWordNode?.type == .Special {
                    gameEnded()
                } else {
                    AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
                    updateScore(-1)
                    textField.text = currentlyTypedWordNode!.typedWord
                    isPerfectWord = false
                }
            }
        }
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        guard let touch = touches.first else {
            return
        }
        
        if allowTapping {
            shootProjectile(touch.locationInNode(self), name: "tap")
        }
    }
    
    func removeWordNode(wordNode : ASAttributedLabelNode) {
        
        shootProjectile(wordNode.position)
        
        if wordNode.type == .Special {
            addExplosion()
            removeAllWords()
            
            removeActionForKey(GenerateWordsActionKey)

            generateWordsAction = SKAction.repeatActionForever(
                SKAction.sequence([
                    SKAction.waitForDuration(delayBetweenWords),
                    SKAction.runBlock(addWord)
                    ])
            )
            
            runAction(generateWordsAction, withKey: GenerateWordsActionKey)
        }
    }
    
    func removeAllWords() {
        for node in self.children {
            if node is ASAttributedLabelNode {
                if node != previouslyTypedWordNode {
                    node.removeFromParent()
                }
            }
        }
        currentWords.removeAll()
        textField.text = ""
    }
    
    func shootProjectile(position : CGPoint, name : String = "") {
        
        if NSUserDefaults.standardUserDefaults().boolForKey(VolumeUserDefaultKey) == true {
            runAction(SKAction.playSoundFileNamed("Laser", waitForCompletion: false))
        }
        
        let angle = atan2((position.x - player.position.x) , (position.y - player.position.y))
        player.zRotation = CGFloat(-angle)
        
        let projectile = SKSpriteNode(imageNamed: "planet")
        projectile.size = UIDevice().isIpad() ? CGSizeMake(50, 50) : CGSizeMake(35, 35)
        projectile.name = name
        projectile.position = player.position
        let offset = position - projectile.position
        
        addChild(projectile)
        
        let direction = offset.normalized()
        let shootAmount = direction * 1000
        let destination = shootAmount + projectile.position
        let actionMove = SKAction.moveTo(destination, duration: 1.25)
        let actionMoveDone = SKAction.removeFromParent()
        projectile.runAction(SKAction.sequence([actionMove, actionMoveDone]))
        
        projectile.physicsBody = SKPhysicsBody(circleOfRadius: projectile.size.width/2)
        projectile.physicsBody?.dynamic = true
        projectile.physicsBody?.categoryBitMask = PhysicsCategory.Projectile
        projectile.physicsBody?.contactTestBitMask = PhysicsCategory.Word
        projectile.physicsBody?.collisionBitMask = PhysicsCategory.None
        projectile.physicsBody?.usesPreciseCollisionDetection = true
    }
    
    //SKContactDelegate
    func didBeginContact(contact: SKPhysicsContact) {
        var firstBody, secondBody : SKPhysicsBody
        
        //Order physics bodies in order of their category bitmask values
        if contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask {
            firstBody = contact.bodyA
            secondBody = contact.bodyB
        } else {
            firstBody = contact.bodyB
            secondBody = contact.bodyA
        }
        
        //Make sure that a word and projectile have collided
        if ((firstBody.categoryBitMask == PhysicsCategory.Word) &&
            (secondBody.categoryBitMask == PhysicsCategory.Projectile))  {
            if let secondNode = secondBody.node, let firstNode = firstBody.node {
                projectileDidCollideWithMonster(firstNode as! ASAttributedLabelNode, projectile: secondNode)
            }
        }
    }
    
    func projectileDidCollideWithMonster(wordNode : ASAttributedLabelNode, projectile : SKNode) {
        
        if currentlyTypedWordNode == wordNode {
            textField.text = ""
            currentlyTypedWordNode = nil
        }
        
        if currentWords.contains(wordNode.name!) {
            currentWords.removeAtIndex(currentWords.indexOf(wordNode.name!)!)
        }
        
        if wordNode != previouslyTypedWordNode && projectile.name != "tap" {
            return
        }
        
        if isPerfectWord {
            updateScore(wordNode.word.characters.count + bonusScoreForPerfectWord)
        } else {
            updateScore(wordNode.word.characters.count)
        }
        
        projectile.removeFromParent()
        wordNode.removeFromParent()
        wordsCompleted += 1
        
        if perfectWordStreak >= perfectWordStreakValue {
            tapLabel.alpha = 1.0
            updatePerfectWordStreak(0)
            
            if NSUserDefaults.standardUserDefaults().boolForKey(VolumeUserDefaultKey) == true {
                if backgroundMusic != nil {
                    backgroundMusic.removeFromParent()
                }
                
                let tempAudio = SKAudioNode(fileNamed: "perfect_word_tapping.wav")
                tempAudio.autoplayLooped = true
                tappingMusic = tempAudio
                addChild(tappingMusic)
            }
            
            allowTapping = true
            
            self.performSelector(#selector(GameScene.stopTapping), withObject: self, afterDelay: timeAllowedForTapping)
        } 
    }
    
    func stopTapping() {
        tapLabel.alpha = 0.0
        allowTapping = false
        
        if NSUserDefaults.standardUserDefaults().boolForKey(VolumeUserDefaultKey) == true {
            tappingMusic.removeFromParent()
            addChild(backgroundMusic)
        }
    }
    
    func specialWordEnded() {
        if currentlyTypedWordNode?.type == .Special {
            self.gameEnded()
        } else {
            specialWordNode!.removeFromParent()
        }
    }
    
    func gameEnded() {
        if DataManager.sharedInstance.gcEnabled {
            updateGameCenter()
        }
        checkForHighScore()
        
        for view in self.view!.subviews {
            view.removeFromSuperview()
        }
        specialWordTimer?.invalidate()
        specialWordTimer = nil
        self.removeAllActions()
        self.removeAllChildren()
        self.sceneDelegate?.gameEnded(score, wordNode: currentlyTypedWordNode)
    }
    
    func checkForHighScore() {
        let userDefaults = NSUserDefaults.standardUserDefaults()
        var highScoreKey : String!
        
        switch difficulty {
        case .Easy:
            highScoreKey = HighScoreUserDefaultKeys[0]
            break
        case .Medium:
            highScoreKey = HighScoreUserDefaultKeys[1]
            break
        case .Hard:
            highScoreKey = HighScoreUserDefaultKeys[2]
            break
        }
        
        let highScore = userDefaults.integerForKey(highScoreKey)
        
        if score > highScore {
            userDefaults.setInteger(score, forKey: highScoreKey)
        }
    }
    
    func updateGameCenter() {
        var leaderboardID : String
        
        switch difficulty {
        case .Easy:
            leaderboardID = gameCenterLeaderBoardKeys[0]
            break
        case .Medium:
            leaderboardID = gameCenterLeaderBoardKeys[1]
            break
        case .Hard:
            leaderboardID = gameCenterLeaderBoardKeys[2]
            break
        }
        
        let sScore = GKScore(leaderboardIdentifier: leaderboardID)
        sScore.value = Int64(score)
        
        GKScore.reportScores([sScore], withCompletionHandler: { (error: NSError?) -> Void in
            if error != nil {
                print(error!.localizedDescription)
            } else {
                print("Score submitted")
            }
        })
    }
    
    func addExplosion() {
        //play explosion sound
        
        //shake scene
        let moveX1 = SKAction.moveBy(CGVectorMake(-7, 0), duration: 0.05)
        let moveX2 = SKAction.moveBy(CGVectorMake(-10, 0), duration: 0.05)
        let moveX3 = SKAction.moveBy(CGVectorMake(7, 0), duration: 0.05)
        let moveX4 = SKAction.moveBy(CGVectorMake(10, 0), duration: 0.05)

        let moveY1 = SKAction.moveBy(CGVectorMake(0, -7), duration: 0.05)
        let moveY2 = SKAction.moveBy(CGVectorMake(0, -10), duration: 0.05)
        let moveY3 = SKAction.moveBy(CGVectorMake(0, 7), duration: 0.05)
        let moveY4 = SKAction.moveBy(CGVectorMake(0, 10), duration: 0.05)
        
        let trembleX = SKAction.sequence([moveX1, moveX2, moveX3, moveX4, moveX1, moveX2, moveX3, moveX4])
        let trembleY = SKAction.sequence([moveY1, moveY2, moveY3, moveY4, moveY1, moveY2, moveY3, moveY4])
        
        for child in children {
            child.runAction(trembleX)
            child.runAction(trembleY)
        }
        
        let fireEmitter = NSKeyedUnarchiver.unarchiveObjectWithFile(NSBundle.mainBundle().pathForResource("spark", ofType: "sks")!)! as! SKEmitterNode
        fireEmitter.position = previouslyTypedWordNode!.position
        fireEmitter.targetNode = self
        addChild(fireEmitter)
        fireEmitter.runAction(SKAction.sequence([SKAction.waitForDuration(0.1),
                                                SKAction.scaleBy(1.5, duration: 0.1),
            SKAction.runBlock({
                fireEmitter.particleBirthRate = 0
            })]))
        
        let smokeEmitter = NSKeyedUnarchiver.unarchiveObjectWithFile(NSBundle.mainBundle().pathForResource("smoke", ofType: "sks")!)! as! SKEmitterNode
        smokeEmitter.position = previouslyTypedWordNode!.position
        smokeEmitter.targetNode = self
        addChild(smokeEmitter)
        smokeEmitter.runAction(SKAction.sequence([SKAction.waitForDuration(0.2),
            SKAction.scaleBy(1.5, duration: 0.1),
            SKAction.runBlock({
                smokeEmitter.particleBirthRate = 0
            })]))
    }
}
